package AppState;

import EstadosState.Descontrolado;
import EstadosState.Desobediente;
import EstadosState.Estado;
import EstadosState.Feliz;
import EstadosState.Latindo;
import EstadosState.Mansinho;
import EstadosState.Medroso;
import EstadosState.Pulando;

public class Jimy {

	protected Estado estado;
	public Descontrolado descontrolado;
	public Feliz feliz;
	public Latindo latindo;
	public Mansinho mansinho;
	public Medroso medroso;
	public Pulando pulando;
	public Desobediente desobediente;

	public Jimy() {
		estado = new Mansinho(this);

	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Estado getEstado() {
		return estado;
	}

	public void chamarJimy() {
		this.estado.chamarJimy();
	}

	public void chegarEmCasa() {
		this.estado.chegarEmCasa();

	}

	public void soltarFogos() {
		this.estado.soltarFogos();

	}

	public void gatosNoMuro() {
		this.estado.gatosNoMuro();

	}

	public void fazerCarinho() {
		this.estado.fazerCarinho();
	}

	public void darComida() {
		this.estado.darComida();
	}
	
	public void sairParaPassear(){
		this.estado.sairParaPassear();
	}

	public Descontrolado getDescontrolado() {
		return descontrolado;
	}

	public Feliz getFeliz() {
		return feliz;
	}

	public Latindo getLatindo() {
		return latindo;
	}

	public Mansinho getMansinho() {
		return mansinho;
	}

	public Medroso getMedroso() {
		return medroso;
	}

	public Pulando getPulando() {
		return pulando;
	}

	public Desobediente getDesobediente() {
		return desobediente;
	}

}
