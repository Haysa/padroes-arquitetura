package EstadosState;

public interface Estado {

	void chamarJimy();
	void chegarEmCasa();
	void soltarFogos();
	void gatosNoMuro();
	void fazerCarinho();
	void darComida();
	void sairParaPassear();
}
