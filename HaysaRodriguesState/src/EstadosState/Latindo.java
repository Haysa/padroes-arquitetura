package EstadosState;

import AppState.Jimy;

public class Latindo implements Estado {

	private Jimy jimy;

	public Latindo(Jimy jimy) {
		this.jimy = jimy;
	}

	@Override
	public void chamarJimy() {
		System.out
				.println("Au, au, au! Sou um cachorro saltitante quando me chamam!");
		this.jimy.setEstado(this.jimy.getPulando());
	}

	@Override
	public void chegarEmCasa() {
		System.out
				.println("Eita, meus donos chegaram em casa! Vou correr DESCONTROLADAMENTE em volta da mesa!");
		this.jimy.setEstado(this.jimy.getDescontrolado());

	}

	@Override
	public void soltarFogos() {
		System.out
				.println("Odeio esses fogos. Viro um cachorro medroso com esses pipocos.. arg arg..");
		this.jimy.setEstado(this.jimy.getMedroso());
	}

	@Override
	public void gatosNoMuro() {
		System.out
				.println("AU, AU AU, AU. Vou pegar voc�s seus gatos chatos. UAUAUAAUAUAU");
		this.jimy.setEstado(this.jimy.getLatindo());
	}

	@Override
	public void fazerCarinho() {
		System.out
				.println("Hummm..est�o fazendo carinho em mim! Estou bem feliz agora.");
		this.jimy.setEstado(this.jimy.getFeliz());
	}

	@Override
	public void darComida() {
		System.out
				.println("OBA! Agora estou bem melhor com comida. Como tudo r�pido e fico tranquil�o.");
		this.jimy.setEstado(this.jimy.getMansinho());
	}

	public void sairParaPassear() {
		System.out
				.println("Me transformei!! Sou desobediente quando eu saio, to nem a�!");
		this.jimy.setEstado(this.jimy.getDesobediente());
	}

	public String toString() {
		return " AUA AU AU AU!";
	}

}
