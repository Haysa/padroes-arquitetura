package PatinsFactory;

import ProdutosDaFactory.PatinsDeBrinquedo;
import ProdutosDaFactory.PatinsFitness;
import ProdutosDaFactory.PatinsSlalom;
import RegrasDaFactory.EnumTipoPatins;
import RegrasDaFactory.MontagemDoPatins;
import RegrasDaFactory.SimpleFactory;

public class TraxartPatinsFactory extends SimpleFactory {

	@Override
	public MontagemDoPatins montarPatins(EnumTipoPatins tipoPatins) {
		MontagemDoPatins m = null;
		switch (tipoPatins) {
		case PATINS_DE_BRINQUEDO:
			return new PatinsDeBrinquedo();

		case FITNESS:
			return new PatinsFitness();

		case SLALOM:
			return new PatinsSlalom();

		}

		return m;
	}

}
