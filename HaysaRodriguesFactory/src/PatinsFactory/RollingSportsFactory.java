package PatinsFactory;

import ProdutosDaFactory.PatinsSpeed;
import ProdutosDaFactory.PatinsStreet;
import RegrasDaFactory.EnumTipoPatins;
import RegrasDaFactory.MontagemDoPatins;
import RegrasDaFactory.SimpleFactory;

public class RollingSportsFactory extends SimpleFactory {

	@Override
	public MontagemDoPatins montarPatins(EnumTipoPatins tipoPatins) {
		MontagemDoPatins mon = null;

		switch (tipoPatins) {

		case SPEED:
			return new PatinsSpeed();

		case STREET:
			return new PatinsStreet();

		}

		return mon;
	}

}
