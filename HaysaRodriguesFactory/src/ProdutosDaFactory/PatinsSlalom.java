package ProdutosDaFactory;

import RegrasDaFactory.MontagemDoPatins;

public class PatinsSlalom extends MontagemDoPatins {

	public PatinsSlalom() {
		super.tipoDoPatins = "## Patins Slalom: ";
		super.preco = 600.00;
	}
}
