package ProdutosDaFactory;

import RegrasDaFactory.MontagemDoPatins;

public class PatinsStreet extends MontagemDoPatins {

	public PatinsStreet() {
		super.tipoDoPatins = "## Patins Street: ";
		super.preco = 1500.00;
	}
}
