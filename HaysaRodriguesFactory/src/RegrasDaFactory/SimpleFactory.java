package RegrasDaFactory;

public abstract class SimpleFactory {

	public MontagemDoPatins getMontagem(EnumTipoPatins tipoPatins) {
		MontagemDoPatins m = montarPatins(tipoPatins);

		m.montarPatins();
		m.colocarRodinhas();
		m.colocarBelezaNoPatins();
		m.colocarPrecoNoPatins();
		return m;
	}

	public abstract MontagemDoPatins montarPatins(EnumTipoPatins tipoPatins);

}
