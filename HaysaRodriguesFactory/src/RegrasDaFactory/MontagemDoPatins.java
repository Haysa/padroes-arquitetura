package RegrasDaFactory;

public abstract class MontagemDoPatins {

	protected String tipoDoPatins;
	protected double preco;

	public String getTipoDoPatins() {
		return tipoDoPatins;
	}

	public double getPreco() {
		return preco;
	}

	public void montarPatins() {
		System.out
				.println(tipoDoPatins
						+ "Montando o melhor patins para esse modelo com nossa experi�ncia.");
	}

	public void colocarRodinhas() {
		System.out
				.println(tipoDoPatins
						+ "Com o conjunto de rodinhas perfeito para esse modelo e o melhor rolamento para esse tipo de patins.");
	}

	public void colocarBelezaNoPatins() {
		System.out
				.println(tipoDoPatins
						+ "Montanto uma pintura radical junto com cores e desenhos adequados para cada idade e cada gosto.");
	}

	public void colocarPrecoNoPatins() {
		System.out.println(tipoDoPatins
				+ "Para esse tipo de patins, nosso melhor pre�o �: " + preco);

	}
}
