package RunApp;

import PatinsFactory.RollingSportsFactory;
import PatinsFactory.TraxartPatinsFactory;
import RegrasDaFactory.EnumTipoPatins;
import RegrasDaFactory.MontagemDoPatins;
import RegrasDaFactory.SimpleFactory;

public class App {

	public void testeFactoryTraxart() {

		SimpleFactory factoryTraxart = new TraxartPatinsFactory();

		System.out.println("**   Somos a Traxart, conhe�a nossa linha de patins  ** \n");

		MontagemDoPatins m = factoryTraxart.getMontagem(EnumTipoPatins.PATINS_DE_BRINQUEDO);
		System.out.println("\n");

		m = factoryTraxart.getMontagem(EnumTipoPatins.FITNESS);
		System.out.println("\n");
		
		m = factoryTraxart.getMontagem(EnumTipoPatins.SLALOM);
		System.out.println("\n");
		
	}
	
	public void testeFactoryRollingSports(){
		
		SimpleFactory factoryRS = new RollingSportsFactory();
		
		System.out.println("**  Somos a Rolling Sports, conhe�a nossas melhores op��es de patins  ** \n");
		
		MontagemDoPatins n = factoryRS.getMontagem(EnumTipoPatins.SPEED);
		System.out.println("\n");
		
		n = factoryRS.getMontagem(EnumTipoPatins.STREET);
		
		
	}

}
