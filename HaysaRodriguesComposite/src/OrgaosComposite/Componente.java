package OrgaosComposite;

public abstract class Componente {

	public void colocarOrgao(Componente componente) {
		throw new UnsupportedOperationException();
	}

	public void retirarOrgao(Componente componente) {
		throw new UnsupportedOperationException();
	}

	public Componente pegarOrgao(double orgao) {
		throw new UnsupportedOperationException();
	}

	public void operation() {
		throw new UnsupportedOperationException();

	}

	public int getOrgao() {
		throw new UnsupportedOperationException();
	}

}
