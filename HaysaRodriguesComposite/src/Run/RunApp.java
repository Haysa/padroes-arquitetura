package Run;

import Orgaos.BancoDeOrgaos;
import Orgaos.Isopor;
import Orgaos.Orgao;

public class RunApp {

	public static void main(String[] args) {

		BancoDeOrgaos banco = new BancoDeOrgaos("Banco de org�os Hospital das Cl�nicas.\n");
		
		/**
		 * Colcando org�os no banco de org�os
		 */
		banco.printBanco();
		Isopor isopor1 = new Isopor();
		Orgao orgao1 = new Orgao(1, "F�gado\n");
		isopor1.colocarOrgao(orgao1);		
		Orgao orgao2 = new Orgao(1, "Cora��o\n");
		isopor1.colocarOrgao(orgao2);
		Orgao orgao3 = new Orgao(2, "P�ncreas\n");
		isopor1.colocarOrgao(orgao3);
		Orgao orgao4 = new Orgao(4, "Rim\n");
		isopor1.colocarOrgao(orgao4);
		isopor1.operation();
		
		/**
		 * Retirando os rims do banco para doa��o
		 */
		System.out.println("\nOs rims foram levados para doa��o.");
		isopor1.retirarOrgao(orgao4);
		isopor1.operation();

	}

}
