package Orgaos;

import OrgaosComposite.Componente;

public class Orgao extends Componente {

	private int orgao;
	private String nomeOrgao;

	public Orgao(int orgao, String nomeOrgao) {
		this.orgao = orgao;
		this.nomeOrgao = nomeOrgao;
	}

	public int getOrgao() {
		return orgao;

	}

	public void operation() {
		System.out.println("Quantidade do org�o: " + orgao + "\n" + "Nome do org�o doado: " + nomeOrgao);
	}

}
