package Orgaos;

import java.util.ArrayList;

import OrgaosComposite.Componente;

public class Isopor extends Componente {

	private ArrayList<Componente> comp;

	public Isopor() {
		comp = new ArrayList<Componente>();
	}

	public void colocarOrgao(Componente componente) {
		comp.add(componente);
	}

	public void retirarOrgao(Componente componente) {
		comp.remove(componente);
	}

	public Componente pegarOrgao(int item) {
		return comp.get(item);

	}

	public void operation() {
		int totalOrgaos = 0;
		String msg = "";
		for (Componente c : comp) {
			c.operation();
			try {
				totalOrgaos += c.getOrgao();
			} catch (UnsupportedOperationException e) {

			}
		}

		System.out.println("\n ----Total de org�os no estoque: " + totalOrgaos);
	}

}
