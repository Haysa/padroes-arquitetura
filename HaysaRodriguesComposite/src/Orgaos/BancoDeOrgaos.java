package Orgaos;

import java.util.ArrayList;

import OrgaosComposite.Componente;

public class BancoDeOrgaos extends Componente{
	
	private ArrayList<Componente> banco = new ArrayList<Componente>();
	private String nomeBancoOrgaos;
	
	public BancoDeOrgaos(String nomeBancoOrgaos){
		this.nomeBancoOrgaos = nomeBancoOrgaos;
	}
	
	public String getNomeBancoOrgaos(){
		return nomeBancoOrgaos;
	}
	
	public void colocarOrgao(Componente componente){
		this.banco.add(componente);
	}
	
	public void retirarOrgao(Componente componente){
		this.banco.remove(componente);
	}
	
	public void printBanco(){
		System.out.println("\n ##No banco de org�os: " +getNomeBancoOrgaos());
	}
	
	
	
	

}
