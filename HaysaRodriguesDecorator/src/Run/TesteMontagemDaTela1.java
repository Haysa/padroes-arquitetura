package Run;

import DecoratorTecnicasEartista.DecoratorArtista;
import DecoratorTecnicasEartista.Crianca;
import DecoratorTecnicasEartista.RomeroBrito;
import FasesDaPinturaNaTela.CoresFortesEDiversas;
import FasesDaPinturaNaTela.DesenhoInicial;
import FasesDaPinturaNaTela.FormasGeometricasLinhasAleatorias;
import FasesDaPinturaNaTela.PinturaComEsponja;
import FasesDaPinturaNaTela.TracosPretosIntensos;

public class TesteMontagemDaTela1 {

	public static void main(String[] args) {

		// teste 1: com o primero artista
		DecoratorArtista romero = new RomeroBrito();
		DesenhoInicial inicio = new DesenhoInicial(romero);
		CoresFortesEDiversas cores = new CoresFortesEDiversas(romero);
		FormasGeometricasLinhasAleatorias formas = new FormasGeometricasLinhasAleatorias(
				romero);
		TracosPretosIntensos tracos = new TracosPretosIntensos(romero);

		System.out.println(" **Come�ando a arte na tela** \n ");
		System.out.println(inicio.getDescricaoTecnica() + "\n"
				+ cores.getDescricaoTecnica() + "\n"
				+ formas.getDescricaoTecnica() + "\n"
				+ tracos.getDescricaoTecnica());

		// teste 2: com o segundo artista
		DecoratorArtista crianca = new Crianca();
		DesenhoInicial inicio2 = new DesenhoInicial(crianca);
		FormasGeometricasLinhasAleatorias formas2 = new FormasGeometricasLinhasAleatorias(
				crianca);
		PinturaComEsponja esponja = new PinturaComEsponja(crianca);

		System.out.println("\n **Agora � a vez da crian�a**");
		System.out.println(inicio2.getDescricaoTecnica() + "\n"
				+ formas2.getDescricaoTecnica() + "\n"
				+ esponja.getDescricaoTecnica());

	}
}
