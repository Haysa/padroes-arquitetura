package FasesDaPinturaNaTela;

import DecoratorTecnicasEartista.DecoratorArtista;

public class TracosPretosIntensos extends DecoratorTecnicasNaTela {

	DecoratorArtista artista;

	public TracosPretosIntensos(DecoratorArtista artista) {
		this.artista = artista;
	}

	@Override
	public String getDescricaoTecnica() {

		return artista.getDescricaoTecnica()
				+ "\n #Contornando os desenhos com tra�os pretos e intensos! ";
	}

}
