package FasesDaPinturaNaTela;

import DecoratorTecnicasEartista.DecoratorArtista;

public class FormasGeometricasLinhasAleatorias extends DecoratorTecnicasNaTela {

	DecoratorArtista artista;

	public FormasGeometricasLinhasAleatorias(DecoratorArtista artista) {
		this.artista = artista;
	}

	@Override
	public String getDescricaoTecnica() {

		return artista.getDescricaoTecnica()
				+ "\n #Desenhando formas geométricas e linhas aleatórias dentro do desenho inicial!";
	}

}
