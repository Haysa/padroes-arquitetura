package FasesDaPinturaNaTela;

import DecoratorTecnicasEartista.DecoratorArtista;

public class DesenhoInicial extends DecoratorTecnicasNaTela {

	DecoratorArtista artista;

	public DesenhoInicial(DecoratorArtista artista) {
		this.artista = artista;
	}

	@Override
	public String getDescricaoTecnica() {

		return artista.getDescricaoTecnica()
				+ "\n #Desenhando o desenho base na tela!";
	}

}
