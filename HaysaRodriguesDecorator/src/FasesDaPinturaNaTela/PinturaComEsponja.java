package FasesDaPinturaNaTela;

import DecoratorTecnicasEartista.DecoratorArtista;

public class PinturaComEsponja extends DecoratorTecnicasNaTela{

	DecoratorArtista artista;

	public PinturaComEsponja(DecoratorArtista artista) {
		this.artista = artista;
	}

	@Override
	public String getDescricaoTecnica() {
		// TODO Auto-generated method stub
		return artista.getDescricaoTecnica()
				+ "\n #Pintando a tela com a t�cnica da esponja molhada com tinta.";
	}

}
