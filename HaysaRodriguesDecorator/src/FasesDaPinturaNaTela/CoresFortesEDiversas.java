package FasesDaPinturaNaTela;

import DecoratorTecnicasEartista.DecoratorArtista;

public class CoresFortesEDiversas extends DecoratorTecnicasNaTela {

	DecoratorArtista artista;

	public CoresFortesEDiversas(DecoratorArtista artista) {
		this.artista = artista; 
	}

	@Override
	public String getDescricaoTecnica() {
		return artista.getDescricaoTecnica()
				+ "\n #Colocando diversas cores fortes no desenho da tela!";
	}

}
