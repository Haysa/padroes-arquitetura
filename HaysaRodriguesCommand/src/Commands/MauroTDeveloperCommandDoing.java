package Commands;

import CommandParticipantes.MauroTeamDeveloper;

public class MauroTDeveloperCommandDoing implements ICommandQuadro {

	private MauroTeamDeveloper mauro;

	public MauroTDeveloperCommandDoing(MauroTeamDeveloper mauro) {
		this.mauro = mauro;
	}

	@Override
	public void comecarTarefa() {
		mauro.arrastarTarefaDoing();

	}
}
