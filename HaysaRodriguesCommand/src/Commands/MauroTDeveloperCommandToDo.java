package Commands;

import CommandParticipantes.MauroTeamDeveloper;

public class MauroTDeveloperCommandToDo implements ICommandQuadro {

	private MauroTeamDeveloper mauro;

	public MauroTDeveloperCommandToDo(MauroTeamDeveloper mauro) {
		this.mauro = mauro;
	}

	@Override
	public void comecarTarefa() {
		mauro.arrastarTarefaToDo();
	}

}
