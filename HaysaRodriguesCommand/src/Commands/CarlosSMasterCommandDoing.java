package Commands;

import CommandParticipantes.CarlosScrumMaster;

public class CarlosSMasterCommandDoing implements ICommandQuadro {

	private CarlosScrumMaster carlos;

	public CarlosSMasterCommandDoing(CarlosScrumMaster carlos) {
		this.carlos = carlos;
	}

	@Override
	public void comecarTarefa() {
		carlos.arrastarTarefaDoing();

	}

}
