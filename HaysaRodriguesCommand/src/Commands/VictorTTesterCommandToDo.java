package Commands;

import CommandParticipantes.VictorTeamTester;

public class VictorTTesterCommandToDo implements ICommandQuadro {

	private VictorTeamTester victor;

	public VictorTTesterCommandToDo(VictorTeamTester victor) {
		this.victor = victor;
	}

	@Override
	public void comecarTarefa() {
		victor.arrastarTarefaToDo();

	}

}
