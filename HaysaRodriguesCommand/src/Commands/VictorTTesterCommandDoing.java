package Commands;

import CommandParticipantes.VictorTeamTester;

public class VictorTTesterCommandDoing implements ICommandQuadro {

	private VictorTeamTester victor;

	public VictorTTesterCommandDoing(VictorTeamTester victor) {
		this.victor = victor;
	}

	@Override
	public void comecarTarefa() {
		victor.arrastarTarefaDoing();

	}
}
