package Commands;

import CommandParticipantes.CarlosScrumMaster;

public class CarlosSMasterCommandToDo implements ICommandQuadro {

	private CarlosScrumMaster carlos;

	public CarlosSMasterCommandToDo(CarlosScrumMaster carlos) {
		this.carlos = carlos;
	}

	@Override
	public void comecarTarefa() {
		carlos.arrastarTarefaToDo();
	}

}
