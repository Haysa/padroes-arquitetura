package Commands;

import CommandParticipantes.ThiagoTeamDeveloper;

public class ThiagoTDeveloperCommandDoing implements ICommandQuadro {

	private ThiagoTeamDeveloper thiago;

	public ThiagoTDeveloperCommandDoing(ThiagoTeamDeveloper thiago) {
		this.thiago = thiago;
	}

	@Override
	public void comecarTarefa() {
		thiago.arrastarTarefaDoing();

	}

}
