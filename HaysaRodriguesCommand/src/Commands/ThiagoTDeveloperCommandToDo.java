package Commands;

import CommandParticipantes.ThiagoTeamDeveloper;

public class ThiagoTDeveloperCommandToDo implements ICommandQuadro {

	private ThiagoTeamDeveloper thiago;

	public ThiagoTDeveloperCommandToDo(ThiagoTeamDeveloper thiago) {
		this.thiago = thiago;
	}

	@Override
	public void comecarTarefa() {
		thiago.arrastarTarefaToDo();
	}

}
