package RunCommand;

import Commands.ICommandQuadro;
import Commands.QuadroNulo;

public class ScrumController {

	private ICommandQuadro[] toDo;
	private ICommandQuadro[] doing;
	private QuadroNulo done;

	public ScrumController() {
		this.toDo = new ICommandQuadro[4];
		this.doing = new ICommandQuadro[4];
		for (int i = 0; i < 4; i++) {
			this.toDo[i] = new QuadroNulo();
			this.doing[i] = new QuadroNulo();
		}
		this.done = new QuadroNulo();
	}

	public void setAtividadeScrum(int atividade, ICommandQuadro toDo,
			ICommandQuadro doing) {
		this.toDo[atividade] = toDo;
		this.doing[atividade] = doing;

	}

	public void comecarAtividade(int atividade) {
		this.toDo[atividade].comecarTarefa();

	}

	public void continuarAtividade(int atividade) {
		this.doing[atividade].comecarTarefa();

	}

	public void terminarAtividade(int atividade) {
		done.terminarTarefa();
	}

	public String toString() {
		System.out.println("\n -- Scrum Controller --");
		for (int i = 0; i < 4; i++) {
			System.out.println("Status: " + i + " " + this.toDo[i].getClass()
					+ " " + this.doing[i].getClass());
		}
		return null;
	}
}
