package RunCommand;

import CommandParticipantes.CarlosScrumMaster;
import CommandParticipantes.MauroTeamDeveloper;
import CommandParticipantes.ThiagoTeamDeveloper;
import CommandParticipantes.VictorTeamTester;
import Commands.CarlosSMasterCommandDoing;

import Commands.CarlosSMasterCommandToDo;
import Commands.MauroTDeveloperCommandDoing;

import Commands.MauroTDeveloperCommandToDo;
import Commands.ThiagoTDeveloperCommandDoing;

import Commands.ThiagoTDeveloperCommandToDo;
import Commands.VictorTTesterCommandDoing;

import Commands.VictorTTesterCommandToDo;

public class RunApp {

	public static void main(String[] args) {

		// Controlador dos pacotes
		ScrumController scrum = new ScrumController();
		scrum.toString();

		// Recursos instanciados
		CarlosScrumMaster carlos = new CarlosScrumMaster();
		MauroTeamDeveloper mauro = new MauroTeamDeveloper();
		ThiagoTeamDeveloper thiago = new ThiagoTeamDeveloper();
		VictorTeamTester victor = new VictorTeamTester();

		// Command para os recursos
		CarlosSMasterCommandToDo toDo = new CarlosSMasterCommandToDo(carlos);
		CarlosSMasterCommandDoing doing = new CarlosSMasterCommandDoing(carlos);
		MauroTDeveloperCommandToDo toDo2 = new MauroTDeveloperCommandToDo(mauro);
		MauroTDeveloperCommandDoing doing2 = new MauroTDeveloperCommandDoing(mauro);
		ThiagoTDeveloperCommandToDo toDo3 = new ThiagoTDeveloperCommandToDo(thiago);
		ThiagoTDeveloperCommandDoing doing3 = new ThiagoTDeveloperCommandDoing(thiago);
		VictorTTesterCommandToDo toDo4 = new VictorTTesterCommandToDo(victor);
		VictorTTesterCommandDoing doing4 = new VictorTTesterCommandDoing(victor);
		
		// setando os comandos das atividades
		scrum.setAtividadeScrum(0, toDo, doing);
		scrum.setAtividadeScrum(1, toDo2, doing2);
		scrum.setAtividadeScrum(2, toDo3, doing3);
		scrum.setAtividadeScrum(3, toDo4, doing4);

		
		scrum.toString();

		System.out.println("\n Atividades iniciadas!");		
		System.out.println("\n # Atividades de Carlos #");
		scrum.comecarAtividade(0);
		scrum.continuarAtividade(0);
		System.out.println("\n #Atividades de Mauro #");
		scrum.comecarAtividade(1);
		scrum.continuarAtividade(1);
		System.out.println("\n #Atividades de Thiago #");
		scrum.comecarAtividade(2);
		scrum.continuarAtividade(2);
		System.out.println("\n #Atividades de Victor #");
		scrum.comecarAtividade(3);
		scrum.continuarAtividade(3);

		scrum.toString();

		System.out.println("\n # Final de sprint e as atividades: ");
		scrum.terminarAtividade(0);
		scrum.terminarAtividade(1);
		scrum.terminarAtividade(2);
		scrum.terminarAtividade(3);
		System.out.println(" \n SPRINT CONCLU�DA COM SUCESSO.");

	}

}
