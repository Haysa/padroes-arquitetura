package Fixacao;

public class TestePedidoFacade {

	public static void main(String[] args) {

		Estoque estoque = new Estoque();
		Financeiro financeiro = new Financeiro();
		PosVenda posVenda = new PosVenda();
		PedidoFacade facade = new PedidoFacade(estoque, financeiro, posVenda);
		Pedido pedido = new Pedido("Notebook", "Rafael",
				"Av da saudade, lalala, rua do sol, n� 23");

		facade.registraPedido(pedido);

	}

}
